# README #

Este projeto contém scripts de integração com o DataBOX. A necessidade surgiu devido ao uso da ferramenta LookerStudio que possui a possibilidade 
de construir um conector customizado. Assim, este conector fará requests REST no DataBox que por sua vez fará conexões JDBC e entregará os dados 
via JSON.

### Pré-requisitos ###

Ler os seguintes documentos:
https://developers.google.com/looker-studio/connector/build
Projeto de exemplo:
https://github.com/googledatastudio/example-connectors/tree/master/npm-downloads/src