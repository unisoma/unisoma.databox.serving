// Copyright 2023 UniSoma Computação.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview Community Connector for extracting DataBOX rows entities using 
 * REST API requests.
 */

var cc = DataStudioApp.createCommunityConnector();
//var DEFAULT_PACKAGE = 'googleapis';

// [START get_config]
// https://developers.google.com/datastudio/connector/reference#getconfig
function getConfig(request) {
  var config = cc.getConfig();

  config
    .newTextInput()
    .setId('query')
    .setName('Enter the SQL Query')
    .setHelpText('Ex: select * from postgresql.public_dataset.bi_projeto');
  

  return config.build();
}


/**
 * Validates config parameters and provides missing values.
 *
 * @param {Object} configParams Config parameters from `request`.
 * @returns {Object} Updated Config parameters.
 */
function validateConfig(configParams) {
  configParams = configParams || {};
  configParams.package = configParams.package || DEFAULT_PACKAGE;

  configParams.package = configParams.package
    .split(',')
    .map(function(x) {
      return x.trim();
    })
    .join(',');

  return configParams;
}


// [START get_schema]
function getFields(request) {
	var fields = cc.getFields();
  var types = cc.FieldType;

  //dynamically create the fields by the query
  //request data here to have the columns?

  //'select * from postgresql.public_dataset.bi_projeto'
  var row = makeRequest(request.configParams.query + ' limit 1')[0];

  for (var key in row) {
    if (typeof row[key] === 'string' || row[key] instanceof String) {
      fields.newDimension()
        .setId(key)
        .setType(types.TEXT);
    } else if (typeof row[key] === 'number' || row[key] instanceof Number) {
      fields.newDimension()
        .setId(key)
        .setType(types.NUMBER);
    } else {
      fields.newDimension()
        .setId(key)
        .setType(types.TEXT);
    }
    
  }

  return fields;
}

// https://developers.google.com/datastudio/connector/reference#getschema
/*
  {
  "configParams": {
    "query": "select * from postgresql.public_dataset.bi_projeto"
  }
}
*/
function getSchema(request) {
  return {schema: getFields(request).build()};
}

/* 
  {
  configParams: object,
  scriptParams: object,
  dateRange: {
    startDate: string,
    endDate: string
  },
  fields: [
    {
      name: Field.name
    }
  ]
}
*/
function getData(request) {
  //request.configParams = validateConfig(request.configParams);
  console.log('starting...');
  
  var requestedFields = getFields(request).forIds(
    request.fields.map(function(field) {
      console.log(field.name);
      return field.name;
    })
  );
  
  try {
		////TODO :: DataBox requesting...
    console.log(request.configParams.query);

    var parsedRespone = makeRequest(request.configParams.query);

		var data = responseToRows(requestedFields, parsedRespone);

    
  } catch (e) {
    cc.newUserError()
      .setDebugText('Error fetching data from API. Exception details: ' + e)
      .setText(
        'The connector has encountered an unrecoverable error. Please try again later, or file an issue if this error persists.'
      )
      .throwException();
  }

  return {
    schema: requestedFields.build(),
    rows: data
  };
}

function responseToRows(requestedFields, response) {
  // Transform parsed data and filter for requested fields
  return response.map(function(data) {
    var row = [];
    requestedFields.asArray().forEach(function (field) {
      console.log(data[field.getId()]);
      return row.push(data[field.getId()]);
    });
    return { values: row };
  });
}

function makeRequest(sqlQuery) {
  var url = 'http://201.49.115.161:49272/databox/no-auth/api/call';

  var reqBody = {
    "user": "unisoma",
    "source": "Looker",
    "password": "",
    "query": sqlQuery,
    "token": "98f39733-c59f-468c-8e65-b940a23d33ac"
  }; 
  
  var options = {
    'method' : 'post',
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify(reqBody)
  };
  //Logger.log(options);
  var response = UrlFetchApp.fetch(url, options);  
  
  var parsedRespone = JSON.parse(response.getContentText());
  
  return parsedRespone;
}

function getResponse() {
  return '[{"project_id":"1","projeto_nome":"UNISOMA","parent_id":"12005","projeto_pai":"Todos Projetos","permite_alocacao":1,"data_estimada_fim":null,"permite_recursos_alterem_billable":1,"projeto_status":"Aberto","projeto_status_enum":1,"project_status_id":"1","tipo_projeto_id":"4","tipo_projeto":"Gestão","folder_clickup":"","data_inicial_real":null,"inicio_garantia":null,"description":"UniSoma","data_cadastro":1212364800000,"data_cadastro_fim":null,"status_vencimento":"Ativo","custo_medio_previsto":null,"esforco_previsto":null,"inicio_contrato":null,"fim_contrato":null,"empresa_cliente":null,"custo_medio_real":54.30647016929736,"status_custo_total":null,"data_ult_aponta":1691798400000,"data_prim_aponta":1222905600000,"natureza_projeto":"","projeto_orcado":"0","alloc_time":6770413.0,"alloc_time_billable":2291.0,"alloc_time_billable_pmo":0.0,"passagem_saldo":null,"projeto_anterior_id":null,"saldo_a_ceder":null,"esforco_com_passagem_saldo":null,"equipe_primer":"","gerente_conta":null,"cc_modification_date":null}]';
}

function isAdminUser() {
  return true;
}


